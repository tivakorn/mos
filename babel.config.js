module.exports = function (api) {

    api.cache(true)

    const presets = [
        [
            '@babel/preset-env',
            {
                'targets': { 'browsers': ['last 2 chrome versions'] },
                'corejs': '3',
                'useBuiltIns': 'usage'
            }
        ]
    ]

    const plugins = [
        '@babel/plugin-proposal-object-rest-spread'
    ]

    return {
        compact: false,
        presets,
        plugins
    }
}
