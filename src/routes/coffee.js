// Libs
import express from 'express'

// Middleware

// Controllers

const router = express.Router()

// Menu
router.get('/menu', [], [])
router.get('/menu:menu_id', [], [])
router.post('/menu', [], [])
router.put('menu:menu_id', [], [])
router.delete('menu:menu_id', [], [])

// Type

// Composition

export default router