// Libs
import _ from 'lodash'
import objectMapper from 'object-mapper'

// Utils
import { GENERAL } from './errorMessage'

export const selectFields = async (data, schema, fields = '') => {

    let blueprint = _.invert(schema)

    if (fields !== '') {
        blueprint = await mapFields(fields, schema)
    }

    return mapDBtoRespone(data, blueprint)

}

export const mapFields = (selectFields, dataDic) => {

    let fields = selectFields.split(',')
    let queryMap = {}

    fields.map(element => {
        let key = element.trim()
        if (dataDic[key] != undefined) {
            queryMap = { ...queryMap, [dataDic[key]]: key }
        }
    })

    return queryMap
}

export const mapDBtoRespone = (data, blueprint) => {

    const result = []

    if (_.isArray(data)) {

        data.map((item) => {
            const dest = objectMapper(item, blueprint)
            result.push(dest)
        })

        return result

    } else {

        const dest = objectMapper(data, blueprint)

        return dest
    }
}

export const errorResponse = (data, options) => {

    let { error_code, error_message } = data

    if (typeof error_code === typeof undefined || typeof error_message === typeof undefined) {
        error_code = GENERAL.DEFAULT.error_code
        error_message = GENERAL.DEFAULT.error_message
    }

    return {
        success: false,
        error_code,
        error_message,
        ...options
    }

}

export const successResponse = (data = { data: [] }) => {
    return {
        success: true,
        ...data
    }
}
