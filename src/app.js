// Libs
import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import config from 'config'

// Route
import coffeeRoute from './routes/coffee'

const app = express()

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use('/healthcheck', (req, res) => res.status(200).json({ success: true }))

app.use('coffee', coffeeRoute)

app.listen(config.nodejs.port, () => console.log(`nimda-service port : ${config.nodejs.port}`))
