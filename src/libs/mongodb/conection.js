// Libs
import { mongodb } from 'config'
import { MongoClient } from 'mongodb'

const { url, port, dbname, username, password } = mongodb

export default (callback) => {

    let conn = null

    try {
        // connection is open once
        if (typeof username === 'undefined' || typeof password === 'undefined') {
            // without authentication
            conn = await MongoClient.connect(`mongodb://${url}:${port}`, { useNewUrlParser: true, useUnifiedTopology: true })
        } else {
            // authenticate DB
            conn = await MongoClient.connect(`mongodb://${username}:${password}@${url}:${port}/${dbname}`, { useNewUrlParser: true, useUnifiedTopology: true })
        }

        return await callback(conn.db(dbname))

    } catch (e) {
        throw e
    } finally {
        if (conn !== null) conn.close()
    }

}
